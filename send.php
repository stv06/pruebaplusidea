<?php

// Envio de formulario a correo electronico

if( isset($_POST['email']) ){
	
$email_to = "info@plusidea.co";
$email_subject = "Contacto desde plusidea";

if( $_POST['name'] == "" || $_POST['email'] == "" || $_POST['message'] == "" ){
	echo "<span style='color: #C00;'>Por favor, llene todos los campos.</span>";
	die();
}

$email_from = $_POST['email'];
$email_message = "Detalles del contacto: \n\n";
$email_message .= "Nombre: " . $_POST['name'] . "\n";
$email_message .= "Email: " . $_POST['email'] . "\n";
$email_message .= "Mensaje: " . $_POST['message'] . "\n\n";

$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n".
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);

echo "<span style='color: #3C0;'>Mensaje enviado con éxito, gracias.</span>";

}

?>