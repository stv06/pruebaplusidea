$(function() {

// Validar formulario con Ajax
$("#contact-form").on("submit", function(){
	var name = $("#name");
	var email = $("#email");
	var message = $("#message");
	var url = "send.php";
	$.ajax({
		type: "POST",
		url: url,
		data: $("#contact-form").serialize(),
		success: function(data){
			$("#respond").html(data);
		}
	})
	name.val("");
	email.val("");
	message.val("");
	return false;
});

// Animacion del menu cuando se hace Scroll
menu = $("#navbar-top-animation");
menuLinks = menu.find("a");
menuTop = menu.offset().top;

$(window).on("scroll", function(){
	var scrollTop = $(window).scrollTop();

	if(scrollTop >= 10){
		menu.addClass("nav-bg");
		menuLinks.addClass("nav-links-color");
	}else{
		menu.removeClass("nav-bg");
		menuLinks.removeClass("nav-links-color");
	}
});

// Animacion del menu responsive cuando se hace click
$("#navbar-top-btn").on("click", function(){
	if( !menu.hasClass("nav-bg") || !menuLinks.hasClass("nav-links-color") ){
		menu.toggleClass("nav-bg");
		menuLinks.toggleClass("nav-links-color");
	}
});

// Animacion cuando se hace click en el boton de "Order Now"
$('a.page-scroll').bind('click', function(event) {
	var $anchor = $(this);
	$('html, body').stop().animate({
		scrollTop: $($anchor.attr('href')).offset().top
	}, 1500);
	event.preventDefault();
});

});



